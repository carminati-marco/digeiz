import React from "react";

const HintContent = ({ value, hideStep = false }) => (
  <div>
    <span>KPI</span>
    <ul>
      <li>
        <b>User</b>: {value.id}
      </li>
      {!hideStep && (
        <li>
          <b>Step</b>: {value.step}
        </li>
      )}

      <li>
        <b>Speed (m/s)</b>: {value.speed}
      </li>
      <li>
        <b>Time</b>: {value.time}
      </li>
    </ul>
  </div>
);

export default HintContent;
