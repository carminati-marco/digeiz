import React from "react";
import { mount } from "enzyme";
import HintContent from "./index";

describe("HintContent test", () => {
  let component;

  beforeEach(async () => {
    const value = { x: 0.5, y: 1, step: 1, label: "label", id: 12, speed: 4.5 };
    component = mount(<HintContent value={value} />);
  });

  it("renders correctly", () => {
    expect(component).toMatchSnapshot();
  });
});
