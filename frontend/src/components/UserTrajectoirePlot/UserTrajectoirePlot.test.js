import React from "react";
import { mount } from "enzyme";
import UserTrajectoirePlot from "./index";

describe("UserTrajectoirePlot test", () => {
  let component;

  beforeEach(async () => {
    component = mount(<UserTrajectoirePlot />);
  });

  it("renders correctly", () => {
    expect(component).toMatchSnapshot();
  });
});
