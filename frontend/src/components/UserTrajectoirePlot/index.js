import React, { Component } from "react";

import { XYPlot, XAxis, YAxis, Hint, VerticalGridLines, HorizontalGridLines, LineMarkSeries } from "react-vis";
import { Dropdown, Header, Icon } from "semantic-ui-react";
import { getAllTrajectoires, getUserOptions } from "../../services/local/localData";
import HintContent from "../HintContent";

import "../TrajectoirePlot.css";

class UserTrajectoirePlot extends Component {
  constructor(props) {
    super(props);
    this.state = { trajectoires: [], selectedValue: null };
  }

  componentDidMount() {
    const trajectoires = getAllTrajectoires();
    this.setState({ trajectoires });
  }

  setValue(value) {
    this.setState({ selectedValue: value });
  }

  setUser(value) {
    const trajectoires = getAllTrajectoires(value);
    this.setState({ trajectoires });
  }

  clearValue() {
    this.setState({
      selectedValue: null
    });
  }

  render() {
    const { selectedValue, trajectoires } = this.state;

    return (
      <div>
        <Header as="h2">
          <Icon name="users" />
          <Header.Content>
            Users
            <Header.Subheader>Filter your data using the user id.</Header.Subheader>
          </Header.Content>
        </Header>
        <div>
          <Dropdown
            placeholder="Select user"
            selection
            clearable
            onChange={(event, { value }) => this.setUser(value)}
            options={getUserOptions()}
          />
          <XYPlot width={600} height={600} xDomain={[0, 10]} yDomain={[0, 10]} dontCheckIfEmpty>
            <XAxis />
            <YAxis />
            <VerticalGridLines />
            <HorizontalGridLines />
            {trajectoires.map(trajectoire => (
              <LineMarkSeries
                key={trajectoire.id}
                className="linemark-series-example"
                style={{
                  strokeWidth: "3px"
                }}
                onValueMouseOver={value => this.setValue(value)}
                onValueMouseOut={() => this.clearValue()}
                data={trajectoire.points}
              />
            ))}

            {selectedValue ? (
              <Hint value={selectedValue}>
                <HintContent value={selectedValue} />
              </Hint>
            ) : null}
          </XYPlot>
        </div>
      </div>
    );
  }
}

export default UserTrajectoirePlot;
