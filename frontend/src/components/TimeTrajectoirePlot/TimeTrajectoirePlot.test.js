import React from "react";
import { mount } from "enzyme";
import TimeTrajectoirePlot from "./index";

describe("TimeTrajectoirePlot test", () => {
  let component;

  beforeEach(async () => {
    component = mount(<TimeTrajectoirePlot />);
  });

  it("renders correctly", () => {
    expect(component).toMatchSnapshot();
  });
});
