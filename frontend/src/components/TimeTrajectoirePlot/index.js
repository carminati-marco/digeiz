import React, { Component } from "react";

import { XYPlot, XAxis, YAxis, Hint, VerticalGridLines, HorizontalGridLines, MarkSeries } from "react-vis";
import { Input, Header, Icon } from "semantic-ui-react";

import HintContent from "../HintContent";
import { getTrajectoires } from "../../services/local/localData";

import "../TrajectoirePlot.css";

class TimeTrajectoirePlot extends Component {
  constructor(props) {
    super(props);
    this.state = { trajectoires: [], selectedValue: null };
  }

  setValue(value) {
    this.setState({ selectedValue: value });
  }

  setTime(value) {
    const trajectoires = getTrajectoires(value);
    this.setState({ trajectoires });
  }

  clearValue() {
    this.setState({
      selectedValue: null
    });
  }

  render() {
    const { selectedValue, trajectoires } = this.state;

    return (
      <div>
        <Header as="h2">
          <Icon name="time" />
          <Header.Content>
            Time
            <Header.Subheader>Filter your data using the sec.</Header.Subheader>
          </Header.Content>
        </Header>

        <div>
          <Input
            type="number"
            min="0"
            label={{ basic: true, content: "sec" }}
            labelPosition="right"
            placeholder="Enter sec... (ex. 240)"
            onChange={data => this.setTime(data.target.value)}
          />
          <XYPlot width={600} height={600} xDomain={[0, 10]} yDomain={[0, 10]} dontCheckIfEmpty>
            <XAxis />
            <YAxis />
            <VerticalGridLines />
            <HorizontalGridLines />
            <MarkSeries
              onValueMouseOver={value => this.setValue(value)}
              onValueMouseOut={() => this.clearValue()}
              data={trajectoires}
            />
            {selectedValue ? (
              <Hint value={selectedValue}>
                <HintContent hideStep value={selectedValue} />
              </Hint>
            ) : null}
          </XYPlot>
        </div>
      </div>
    );
  }
}

export default TimeTrajectoirePlot;
