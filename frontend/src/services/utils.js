const getSpeed = (obj, i, array) => {
  if (i === 0) {
    return 0;
  }
  const prevObj = array[i - 1];
  const deltaTime = obj.time - prevObj.time;
  const distance = Math.hypot(obj.x - prevObj.x, obj.y - prevObj.y);
  return deltaTime > 0 ? parseFloat(distance / deltaTime).toFixed("2") : 0;
};

export default getSpeed;
