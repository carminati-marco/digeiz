import _ from "lodash";
import memoize from "fast-memoize";
import trajectoiresData from "../../data/trajectoires.json";
import getSpeed from "../utils";

const getPoints = trajectoire =>
  _.sortBy(trajectoire.points, "time").map((obj, i, array) => ({
    ...obj,
    step: i + 1,
    label: trajectoire.id,
    id: trajectoire.id,
    speed: getSpeed(obj, i, array)
  }));

const trajectoires = () =>
  _.flatMap(trajectoiresData, trajectoire => {
    return getPoints(trajectoire);
  });

const userTrajectoires = () =>
  trajectoiresData.map(trajectoire => ({
    id: trajectoire.id,
    label: trajectoire.id,
    points: getPoints(trajectoire)
  }));

const usersOption = () =>
  trajectoiresData.map(item => ({
    key: item.id,
    text: item.id,
    value: item.id
  }));

const trajectoiresMemoized = memoize(trajectoires);
const userTrajectoiresMemoize = memoize(userTrajectoires);

export const getUserOptions = memoize(usersOption);

export const getTrajectoires = time => {
  return _.filter(trajectoiresMemoized(), ["time", parseInt(time, 10)]);
};

export const getAllTrajectoires = userId => {
  return userId
    ? _.filter(userTrajectoiresMemoize(), ["id", userId])
    : userTrajectoiresMemoize();
};
