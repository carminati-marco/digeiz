import React from "react";
import "semantic-ui-css/semantic.min.css";

import TimeTrajectoirePlot from "./components/TimeTrajectoirePlot/index";
import UserTrajectoirePlot from "./components/UserTrajectoirePlot/index";
import "./App.css";
import { Header, Icon } from "semantic-ui-react";

function App() {
  return (
    <div>
      <div className="header">
        <Header as="h2">
          <Icon name="settings" />
          <Header.Content>
            Digeiz
            <Header.Subheader>Shopping centre Analytics. exhaustive, qualified and anonymous.</Header.Subheader>
          </Header.Content>
        </Header>
      </div>

      <div className="container">
        <div className="item">
          <UserTrajectoirePlot />
        </div>

        <div className="item">
          <TimeTrajectoirePlot />
        </div>
      </div>
    </div>
  );
}

export default App;
