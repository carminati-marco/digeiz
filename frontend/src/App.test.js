import React from "react";
import { shallow } from "enzyme";
import App from "./App";

test("shallow without crashing", () => {
  const component = shallow(<App />);
  expect(component).toMatchSnapshot();
});
