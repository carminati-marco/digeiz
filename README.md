## TODO List

- As the test environment has already been setup, improve the tests checking the dropdownlist and the "second picker".
- There are some issues with the rendering in the graphs; check if to replace the library or if some data are wrong and must be managed

## Installation

### Docker
if you want to use [docker](https://docs.docker.com/) and [docker-compose](https://docs.docker.com/compose/), in the project directory just run:

#### `make ultimate`

The project will run at http://localhost:3001/

The tests: you can launch the follow command in docker

#### `docker-compose exec frontend yarn test`

Linting
#### `docker-compose exec frontend yarn lint`


### Local
Using [yarn](https://yarnpkg.com/lang/en/), in the root directory run

### `yarn && yarn start`

The project will run at http://localhost:3000/

Test? Just launch

#### `yarn test` 
or 
#### `yarn test:update` 
to update the snapshot + manage the watch usage

Linting
#### `yarn lint`

